# Filename: Makefile
# Author: Iván Ruvalcaba
# Contact: <ivanruvalcaba[at]disroot[dot]org>
#
# Copyright (C) 2023  Iván Ruvalcaba <ivanruvalcaba[at]disroot[dot]org>
#
# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided the copyright
# notice and this notice are preserved.  This file is offered as-is,
# without any warranty.
#
# See the FSF All-Permissive License for more details
# <https://spdx.org/licenses/FSFAP.html>.

MARKDOWN_DIR?=src/
MARKDOWNLINT_OPTIONS?=--disable MD013 MD053

# Variables utilizadas para colorear los mensajes mostrados en la ayuda.
GREEN  := $(shell tput -Txterm setaf 2)
YELLOW := $(shell tput -Txterm setaf 3)
WHITE  := $(shell tput -Txterm setaf 7)
CYAN   := $(shell tput -Txterm setaf 6)
RESET  := $(shell tput -Txterm sgr0)

all: help

## help:
help: ## Mostrar la ayuda.
	@echo ''
	@echo '«Golang Notes» Makefile.'
	@echo ''
	@echo 'Usage:'
	@echo '  ${YELLOW}make${RESET} ${GREEN}<target>${RESET}'
	@echo ''
	@echo 'Targets:'
	@awk 'BEGIN {FS = ":.*?## "} { \
		if (/^[a-zA-Z_-]+:.*?##.*$$/) {printf "    ${YELLOW}%-20s${GREEN}%s${RESET}\n", $$1, $$2} \
		else if (/^## .*$$/) {printf "  ${CYAN}%s${RESET}\n", substr($$1,4)} \
		}' $(MAKEFILE_LIST)

## lint:
lint: ## Ejecutar el «lintern» a los archivos Markdown en el directorio de contenido.
	# Véase: https://github.com/markdownlint/markdownlint/blob/master/docs/RULES.md
	@echo "Realizar el «linting» de los archivos Markdown ubicados en el directorio «${MARKDOWN_DIR}»…"
	markdownlint $(MARKDOWNLINT_OPTIONS) -- $(MARKDOWN_DIR)

.PHONY: all help lint
