# Fundamentos de Go

Archivos fuente del eBook «Fundamentos de Go».

## Descripción

Anotaciones personales y otras recopilaciones sobre el lenguaje de programación Go.

## Contacto

Si necesita contactar a su autor, por favor refiérase a: [ivanruvalcaba\[at\]disroot\[dot\]org][contacto].

## Licencia

Copyright (c) 2023 — Iván Ruvalcaba. *A no ser que se indique explícitamente lo contrario, el contenido de esta obra se encuentra sujeta a los términos de la licencia [CC0 1.0 Universal | Dedicación de Dominio Público][cc0-1.0].* Licencia dual [FSFAP][GNUAllPermissive]/[LGPLv3+][lgpl-3.0] para el código fuente.

[contacto]: mailto%3Aivanruvalcaba@disroot.org%3Fsubject%3D%5Bfundamentos-de-go%5D%20Contacto%3A
[cc0-1.0]: https://creativecommons.org/licenses/by/4.0/deed
[GNUAllPermissive]: https://www.gnu.org/licenses/license-list.en.html#GNUAllPermissive
[lgpl-3.0]: https://www.gnu.org/licenses/lgpl-3.0.en.html