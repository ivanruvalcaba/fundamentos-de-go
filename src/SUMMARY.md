# Sumario

[Prefacio](./prefacio.md)
[Introducción](./introduccion.md)

- [Módulos, paquetes y dependencias](./20230611222346.md)

# Apéndice

- [El conjunto de herramientas Go](./20230613123442.md)
- [Nombres de paquetes](./20230614132037.md)
