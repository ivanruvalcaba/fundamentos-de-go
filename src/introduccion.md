# Introducción

## Acerca del lenguaje de programación Go

[Go][golang] es un lenguaje de programación de código abierto creado por Google y [distribuido bajo una licencia tipo BSD][golang-licencia]. Una [licencia BSD][licencia-bsd] permite a cualquier persona utilizar Go de forma gratuita, siempre y cuando se conserve el aviso de copyright y no se utilice el nombre de Google para promocionarlo o respaldarlo. Go está fuertemente influenciado por el lenguaje de programación C, no obstante tiene una sintaxis mucho más simple y una mejor seguridad de memoria, además de contar con recolección de basura.

### Diseño del lenguaje Go

El objetivo original de Go era crear un nuevo lenguaje que fuera sencillo, fiable y eficiente. Como ya se ha mencionado, Go está muy influenciado por el lenguaje de programación C. El lenguaje en sí es muy sencillo, con solo 25 [palabras clave][golang-keywords]. Fue construido para integrarse bien con IDEs, pero no para depender de ellos.

Uno de los principales objetivos de Go era hacer frente a algunos de los aspectos negativos del código C++ y Java, conservando al mismo tiempo el rendimiento. El lenguaje tenía que ser sencillo y coherente para gestionar equipos de desarrollo muy grandes.

Las variables se tipan estáticamente y las aplicaciones se compilan rápidamente en binarios enlazados estáticamente. Tener un único binario enlazado estáticamente facilita mucho la creación de contenedores ligeros. Las aplicaciones finales también se ejecutan rápido, con un rendimiento cercano al de C++ y Java y mucho más rápido que lenguajes interpretados como Python. Hay punteros, pero no se permite la aritmética de punteros. Go no se promociona a sí mismo como un lenguaje de programación orientado a objetos, y no tiene formalmente clases en el sentido tradicional; sin embargo, contiene una serie de mecanismos que se asemejan mucho a un lenguaje de programación orientado a objetos. Las interfaces se utilizan mucho, y la composición es el equivalente de la herencia.

Go tiene muchas características interesantes. Una característica que destaca es la concurrencia incorporada. Basta con poner la palabra «go» antes de cualquier llamada a una función, y se generará un hilo ligero para ejecutar la función. Otra característica bastante importante es la gestión de dependencias, que es muy eficiente. La gestión de dependencias es parte de la razón por la que Go compila increíblemente rápido. No reincluye los mismos archivos de cabecera varias veces, como hace C++. Go también incorpora seguridad de memoria, y un recolector de basura se encarga de limpiar la memoria no empleada. La biblioteca estándar de Go también es impresionante. Es moderna y contiene paquetes de redes, HTTP, TLS, XML, JSON, bases de datos, manipulación de imágenes y criptografía. Go también soporta Unicode, lo que permite utilizar todo tipo de caracteres en el código fuente.

El conjunto de herramientas Go es fundamental para el ecosistema. Proporciona herramientas para descargar e instalar dependencias remotas, ejecutar pruebas unitarias y benchmarks, generar código y formatearlo según los estándares de formateo de Go. También incluye el compilador, el enlazador y el ensamblador, que compilan muy rápidamente y permiten una fácil compilación cruzada con únicamente cambiar las variables de entorno `GOOS` y `GOARCH`.

Algunas características fueron excluidas del lenguaje Go. Los genéricos, la herencia, las aserciones, las excepciones, la aritmética de punteros y las conversiones de tipo implícitas se omitieron intencionadamente. Los autores decidieron hacer esto, ya sea porque querían mantener el rendimiento, mantener la especificación del lenguaje lo más simple posible, o porque no se ponían de acuerdo sobre la mejor forma de implementarla —o simplemente porque una característica era demasiado controvertida—. La herencia también se omitió intencionadamente en favor del uso de interfaces y composición. Algunas otras características, como los genéricos, se omitieron porque había demasiado debate sobre su correcta implementación; sin embargo, hicieron su aparición en la versión de [Go 1.18][golang-1.18-genericos]. Los autores reconocieron que es mucho más fácil añadir una característica a un lenguaje que quitarla.

[golang]: https://go.dev/ "Sitio oficial del lenguaje de programación Go"
[golang-licencia]: https://go.dev/LICENSE "The Go LICENSE"
[licencia-bsd]: https://web.archive.org/web/20230625195018/https://es.wikipedia.org/wiki/Licencia_BSD "Acerca de la licencia BSD"
[golang-keywords]: https://web.archive.org/web/20230625195657/https://go.dev/ref/spec#Keywords "The Go Programming Language Specification — Keywords"
[golang-1.18-genericos]: https://web.archive.org/web/20230625223000/https://go.dev/blog/intro-generics "The Go Blog — An Introduction To Generics"
