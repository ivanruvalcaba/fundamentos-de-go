# Prefacio

> Mi primera elección para el título del libro fue «Boring Go» porque, bien escrito, Go es aburrido.
>
> Go tiene un pequeño conjunto de características que no está a la altura de la mayoría de los lenguajes de programación modernos. Los programas Go bien escritos tienden a ser sencillos y a veces un poco repetitivos. No hay herencia, no hay genéricos —al menos no hasta su aparición en la [versión 1.18][golang-1.18-genericos]—, no hay programación orientada a aspectos, no hay sobrecarga de funciones, y ciertamente no hay sobrecarga de operadores. No hay concordancia de patrones, ni parámetros con nombre, ni excepciones. Para horror de muchos, hay *punteros*. El modelo de concurrencia de Go es diferente al de otros lenguajes, pero está basado en ideas de los años 70, al igual que el algoritmo utilizado para su recolector de basura. En resumen, Go parece un retroceso. Y de eso se trata.
>
> Aburrido no significa trivial. Usar Go correctamente requiere una comprensión de cómo sus características están destinadas a encajar entre sí. Aunque puede escribir código Go que se parezca al de Java o Python, no estará contento con el resultado y muy seguramente se preguntará por qué tanto alboroto.
>
> Cuando se trata de construir cosas que perduren, ser aburrido es genial. Nadie quiere ser la primera persona que conduce su coche sobre un puente construido con técnicas no probadas que el ingeniero estimó que eran geniales. El mundo moderno depende del software tanto como de los puentes, quizá más. Sin embargo, muchos lenguajes de programación añaden características sin reflexionar el impacto en la capacidad de mantenimiento del código base. *Go está pensado para construir programas que perduren, programas que sean modificados por docenas de desarrolladores a lo largo de docenas de años.*
>
> Go es aburrido y eso es fantástico. — Jon Bodner.

## Licencia

> La persona que asoció una obra con este resumen ha **dedicado** la obra al dominio público, mediante la renuncia a todos sus derechos a la obra bajo las leyes de derechos autorales en todo el mundo, incluyendo todos los derechos conexos y afines, en la medida permitida por la ley.

<div align="center">
    <a href="https://notbyai.fyi/">
        <img src="img/cc-zero.png" alt="Licencia CC0 1.0 Universal (CC0 1.0) Dedicación de Dominio Público" title="Esta obra ha sido liberada bajo la licencia CC0 1.0 Universal \(CC0 1.0\) Dedicación de Dominio Público">
    </a>
</div>

## AI-free Content

> La Inteligencia Artificial (IA) se entrena utilizando contenidos creados por humanos. Si las personas dejan de producir nuevos contenidos y dependen únicamente de la IA, los contenidos en línea de todo el mundo podrían correr el riesgo de volverse repetitivos y sufrir estancamiento por falta de incentivos para la innovación.

<div align="center">
    <a href="https://notbyai.fyi/">
        <img src="img/Written-By-Human-Not-By-AI-Badge-white@2x.png" alt="Escrito por humanos, no por una Inteligencia Artificial (IA)" title="Escrito por humanos, no por una Inteligencia Artificial (IA)">
    </a>
</div>

## Referencias

[Véase: https://web.archive.org/web/20230624193430/https://wiki.froth.zone/wiki/Referencia_bibliogr%C3%A1fica?lang=es#Cita_de_capítulos_o_fragmentos_de_libros]: #

- Bodner, Jon. «Preface». En: *Learning Go: An Idiomatic Approach to Real-World Go Programming*. Primera edición. [s. l.]: O'Reilly Media, Inc., 2021.
- Leon, John Daniel. «About Go», «The Go toolchain». En: *Security with Go: Explore the power of Golang to secure host, web, and cloud services*. Primera edición. [s. l.]: Packt Publishing Ltd, 2018.
- Bates, Mark. *Go Fundamentals*. Primera edición. [s. l.]: Pearson Education, Inc., 2023.

[golang-1.18-genericos]: https://web.archive.org/web/20230625223000/https://go.dev/blog/intro-generics "The Go Blog — An Introduction To Generics"
[notbyai]: https://notbyai.fyi/
[CC0-1.0]: https://creativecommons.org/publicdomain/zero/1.0/deed.es
